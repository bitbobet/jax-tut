# README #

### Git install ###

* https://git-scm.com/download/win
* Install

### Git clone ###

* Windows Start > Search 'Git Bash' > Open it
* *cd* {path_to_your_eclipse_workspace} (ex: */c/Users/bobeali/workspace*)
* *git clone https://bitbobet@bitbucket.org/bitbobet/jax-tut.git*
* *cd jax-tut*
* *git checkout dom1*

### Eclipse setup ###

* **File** -> **New** -> **Java Project**
* Untick '*Use default location*'
* **Location**: {path_to_your_eclipse_workspace}/jax-tut
* **Project Name** (if not autocompleted): jax-tut
* **Finish**