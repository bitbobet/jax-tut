package com.db.school.jax.stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.db.school.jax.model.Book;

public class StAXExample {

	private static LinkedList<String> stack = new LinkedList<>();
	private static List<Book> books = new ArrayList<>();
	
	public static void main(String[] args) {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = null;
		
		try {
			String filename = "books.xml";
			InputStream input = new FileInputStream(filename);
			/*
			 * In general, 
			 * using the iterator API is recommended because it is more flexible and extensible, 
			 * thereby "future-proofing" your applications.
			 */
			reader = factory.createXMLEventReader(input);
			
			while(reader.hasNext()){
				XMLEvent e = reader.nextEvent();
				if (e.getEventType() == XMLEvent.START_ELEMENT){
					StartElement element = e.asStartElement();
					String name = element.getName().getLocalPart();
					stack.push(name);
					if(name.equals("book")){
						Book book = new Book();
						book.setId(Integer.parseInt(element.getAttributeByName(new QName("id")).getValue()));
						books.add(book);
					}
				}
				
				if (e.getEventType() == XMLEvent.CHARACTERS){
					
					if(books.size() == 0) continue;
					
					Characters element = e.asCharacters();
					String content = element.getData();
					
					String name = stack.peek();
					Book bookObj = books.get(books.size()-1);
					
					if(name.equals("author")){
						bookObj.setAuthor(content);
					}
					
					if(name.equals("title")){
						bookObj.setTitle(content);
					}
					
					if(name.equals("genre")){
						bookObj.setGenre(content);
					}
					
					if(name.equals("price")){
						bookObj.setPrice(Double.parseDouble(content));
					}
					
					if(name.equals("publish_date")){
						try {
							bookObj.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").parse(content));
						} catch (ParseException pe) {
							System.err.println(pe.getMessage());
						}
					}
					
					if(name.equals("description")){
						content = (bookObj.getDescription() == null ? "" : bookObj.getDescription()) + content;
						bookObj.setDescription(content);
					}
				}
				
				if (e.getEventType() == XMLEvent.END_ELEMENT){
					stack.pop();
				}
			}
			//finished parsing
			
			System.out.println("List of books:");
			for (Book book : books) {
				System.out.println(book);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} finally{			
			try {
				if(reader != null ) { reader.close(); }
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
	}

}
