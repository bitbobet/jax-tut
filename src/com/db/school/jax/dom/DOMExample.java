package com.db.school.jax.dom;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.db.school.jax.model.Book;

public class DOMExample {

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		String filename="books.xml";
		//obtain an instance of a factory that can give us a document builder
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		//get an instance of a builder:
		DocumentBuilder db = dbf.newDocumentBuilder();
		
		//use it to parse the specified file:
		Document doc = db.parse(new File(filename));
		
		List<Book> books = parseBooks(doc);
		for(Book book : books){
			System.out.println(book);
		}
	}
	
	public static List<Book> parseBooks(Node doc){
		NodeList nodes = doc.getFirstChild().getChildNodes();
		List<Book> books = new ArrayList<Book>();
		
		for(int i=0; i<nodes.getLength(); i++){
			if(nodes.item(i).getNodeType() == Node.ELEMENT_NODE && nodes.item(i).getNodeName().equals("book")){
				Book book = parseBook(nodes.item(i));
				books.add(book);
			}
		}
		
		return books;
	}
	
	public static Book parseBook(Node bookNode){
		Book bookObj = new Book();
		
		NamedNodeMap attributes = bookNode.getAttributes();
		bookObj.setId(Integer.parseInt(attributes.getNamedItem("id").getNodeValue()));
		
		NodeList nodes = bookNode.getChildNodes();
		for(int i=0; i<nodes.getLength(); i++){
			Node element = nodes.item(i);
			if(element.getNodeType() == Node.ELEMENT_NODE){
				String name = element.getNodeName();
				String content = element.getFirstChild().getTextContent();
				if(name.equals("author")){
					bookObj.setAuthor(content);
				}
				
				if(name.equals("title")){
					bookObj.setTitle(content);
				}
				
				if(name.equals("genre")){
					bookObj.setGenre(content);
				}
				
				if(name.equals("price")){
					bookObj.setPrice(Double.parseDouble(content));
				}
				
				if(name.equals("publish_date")){
					try {
						bookObj.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").parse(content));
					} catch (ParseException e) {
						System.err.println(e.getMessage());
					}
				}
				
				if(name.equals("description")){
					bookObj.setDescription(content);
				}
			}
		}
		
		return bookObj;
		
	}
}