package com.db.school.jax.sax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.db.school.jax.model.Book;

/**
 * 
 * Here is a summary of the key SAX APIs:
 * 
 * <dt><tt>SAXParserFactory</tt></dt> <dd>
 * <p>
 * A <tt>SAXParserFactory</tt> object creates an instance of the parser
 * determined by the system property,
 * <tt>javax.xml.parsers.SAXParserFactory</tt>.
 * </p>
 * </dd> <dt><tt>SAXParser</tt></dt> <dd>
 * <p>
 * The <tt>SAXParser</tt> interface defines several kinds of <tt>parse()</tt>
 * methods. In general, you pass an XML data source and a
 * <tt>DefaultHandler</tt> object to the parser, which processes the XML and
 * invokes the appropriate methods in the handler object.
 * </p>
 * </dd> <dt><tt>SAXReader</tt></dt> <dd>
 * <p>
 * The <tt>SAXParser</tt> wraps a <tt>SAXReader</tt>. Typically, you do not care
 * about that, but every once in a while you need to get hold of it using
 * <tt>SAXParser</tt>'s <tt>getXMLReader()</tt> so that you can configure it. It
 * is the <tt>SAXReader</tt> that carries on the conversation with the SAX event
 * handlers you define.
 * </p>
 * </dd> <dt><tt>DefaultHandler</tt></dt> <dd>
 * <p>
 * Not shown in the diagram, a <tt>DefaultHandler</tt> implements the
 * <tt>ContentHandler</tt>, <tt>ErrorHandler</tt>, <tt>DTDHandler</tt>, and
 * <tt>EntityResolver</tt> interfaces (with null methods), so you can override
 * only the ones you are interested in.
 * </p>
 * </dd> <dt><tt>ContentHandler</tt></dt> <dd>
 * <p>
 * Methods such as <tt>startDocument</tt>, <tt>endDocument</tt>,
 * <tt>startElement</tt>, and <tt>endElement</tt> are invoked when an XML tag is
 * recognized. This interface also defines the methods <tt>characters()</tt> and
 * <tt>processingInstruction()</tt>, which are invoked when the parser
 * encounters the text in an XML element or an inline processing instruction,
 * respectively.
 * </p>
 * </dd> <dt><tt>ErrorHandler</tt></dt> <dd>
 * <p>
 * Methods <tt>error()</tt>, <tt>fatalError()</tt>, and <tt>warning()</tt> are
 * invoked in response to various parsing errors. The default error handler
 * throws an exception for fatal errors and ignores other errors (including
 * validation errors). This is one reason you need to know something about the
 * SAX parser, even if you are using the DOM. Sometimes, the application may be
 * able to recover from a validation error. Other times, it may need to generate
 * an exception. To ensure the correct handling, you will need to supply your
 * own error handler to the parser.
 * </p>
 * </dd> <dt><tt>DTDHandler</tt></dt> <dd>
 * <p>
 * Defines methods you will generally never be called upon to use. Used when
 * processing a DTD to recognize and act on declarations for an unparsed entity.
 * </p>
 * </dd> <dt><tt>EntityResolver</tt></dt> <dd>
 * <p>
 * The <tt>resolveEntity</tt> method is invoked when the parser must identify
 * data identified by a URI. In most cases, a URI is simply a URL, which
 * specifies the location of a document, but in some cases the document may be
 * identified by a URN - a public identifier, or name, that is unique in the web
 * space. The public identifier may be specified in addition to the URL. The
 * <tt>EntityResolver</tt> can then use the public identifier instead of the URL
 * to find the document-for example, to access a local copy of the document if
 * one exists.
 * </p>
 * </dd>
 * 
 * @author The Java Tutorials
 *
 */
public class SAXExample {

	public static void main(String[] args) {
		SAXParserFactory factory = SAXParserFactory.newInstance();

		try {

			String filename = "books.xml";
			InputStream input = new FileInputStream(filename);
			
			//Creates a new instance of a SAXParser:
			SAXParser saxParser = factory.newSAXParser();

			/*
			 * The DefaultHandler class comes with the JDK
			 * and implements: EntityResolver, DTDHandler, ContentHandler, ErrorHandler
			 */
			DefaultHandler handler = new MyHandler();
			/*
			 * When you call the SAXParser.parse() method the SAX parser starts the XML processing.
			 * While processing the XML the SAXParser calls methods in the DefaultHandler/MyHandler subclass
			 */
			saxParser.parse(input, handler);
			
			List<Book> books = ((MyHandler) handler).getBooks();
			System.out.println("List of books:");
			for (Book book : books) {
				System.out.println(book);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

	}

}
