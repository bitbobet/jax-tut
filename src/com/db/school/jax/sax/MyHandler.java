package com.db.school.jax.sax;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.db.school.jax.model.Book;

/**
 * It is the responsibility of the DefaultHandler subclass 
 * to extract any necessary information from the XML via these overridden methods.
 *
 */
public class MyHandler extends DefaultHandler {

	private List<Book> books = new ArrayList<>();
	private LinkedList<String> stack = new LinkedList<>();
	
	public List<Book> getBooks() {
		return books;
	}
	
	@Override
	public void startDocument() throws SAXException {
	}

	@Override
	public void endDocument() throws SAXException {
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		stack.push(qName);

		if(qName.equals("book")){
			Book book = new Book();
			book.setId(Integer.parseInt(attributes.getValue("id")));
			books.add(book);
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if(books.size() == 0) return;
		String name = stack.peek();
		String content = new String(ch, start, length);
		Book bookObj = books.get(books.size()-1);
		if(name.equals("author")){
			bookObj.setAuthor(content);
		}
		
		if(name.equals("title")){
			bookObj.setTitle(content);
		}
		
		if(name.equals("genre")){
			bookObj.setGenre(content);
		}
		
		if(name.equals("price")){
			bookObj.setPrice(Double.parseDouble(content));
		}
		
		if(name.equals("publish_date")){
			try {
				bookObj.setPublishDate(new SimpleDateFormat("yyyy-MM-dd").parse(content));
			} catch (ParseException e) {
				System.err.println(e.getMessage());
			}
		}
		
		if(name.equals("description")){
			content = (bookObj.getDescription() == null ? "" : bookObj.getDescription()) + content;
			bookObj.setDescription(content);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		stack.pop();
	}


	/**
	 * The DefaultHandler class has three methods you can override to handle exceptions encountered during the XML parsing:
	 */
	
	@Override
	public void warning(SAXParseException e) throws SAXException {
		super.warning(e);
	}

	@Override
	public void error(SAXParseException e) throws SAXException {
		super.error(e);
	}

	@Override
	public void fatalError(SAXParseException e) throws SAXException {
		super.fatalError(e);
	}
	
	
	
	
}
