package com.db.school.jax.json;

import java.io.File;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.db.school.jax.jaxb.model.Book;
import com.db.school.jax.jaxb.model.Catalog;

public class JSONExample {

	public static void main(String[] args) {
		
		File file = new File("books.xml");
		try {
	        JAXBContext context = JAXBContext.newInstance(Catalog.class);
	        Unmarshaller reader = context.createUnmarshaller();
	        Catalog catalog = (Catalog) reader.unmarshal(file);
	        
	        JsonBuilderFactory jsonFactory = Json.createBuilderFactory(null);
	        JsonArrayBuilder booksJsonBuilder = jsonFactory.createArrayBuilder();
	        
	        for(Book book : catalog.getBooks()){
	        	JsonObjectBuilder bookJson = jsonFactory.createObjectBuilder()
	        		     .add("id", book.getId())
	        		     .add("title", book.getTitle())
	        		     .add("genre", book.getGenre())
	        		     .add("price", book.getPrice())
	        		     .add("publishDate", book.getPublishDate().getTime())
	        		     .add("description", book.getDescription());
	        	booksJsonBuilder.add(bookJson);
	        }
	        JsonObject catalogJson =  jsonFactory.createObjectBuilder()
	        		.add("catalog", booksJsonBuilder)
	        		.build();
	        
	        try (JsonWriter jsonWriter = Json.createWriter(System.out)) {
	        	  jsonWriter.writeObject(catalogJson);
        	}
	        
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		
	}

}
