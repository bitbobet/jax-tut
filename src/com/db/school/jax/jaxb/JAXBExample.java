package com.db.school.jax.jaxb;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import com.db.school.jax.jaxb.model.Book;
import com.db.school.jax.jaxb.model.Catalog;

public class JAXBExample {

	public static void main(String[] args) {
		File file = new File("books.xml");
		try {
			//JAXBContext provides the entry point to the JAXB API:
	        JAXBContext context = JAXBContext.newInstance(Catalog.class);
	        
	        //This object controls the process of unmarshalling (read xml into java objects)
	        Unmarshaller reader = context.createUnmarshaller();
	        
	        //unmarshal method does the actual unmarshalling of the XML document represented by the root element (catalog)
	        Catalog catalog = (Catalog) reader.unmarshal(file);
	        for(Book book : catalog.getBooks()){
	        	System.out.println(book);
	        }
	        
	        //This object controls the process of marshalling (writing java objects to xml)
			Marshaller writer = context.createMarshaller();
			writer.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			writer.marshal(catalog, System.out);
			
			//Generates the schema documents for this context:
	        context.generateSchema(new SchemaOutputResolver(){
				@Override
				public Result createOutput(String namespaceUri,
						String suggestedFileName) throws IOException {
					return new StreamResult(new File("books.xsd"));
				}
	        	
	        });
	        
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

/**
 * There are two ways of creating JAXBContext for java to schema binding:
 * 
 * by root classes newInstance(Class ...):
 * 	JAXBContext context1 = JAXBContext.newInstance(Catalog.class);
 * 
 * by package, requires jaxb.index file in package com.db.school.jax.jaxb.model:
 * 	JAXBContext context1 = JAXBContext.newInstance(String packageNames);
 * 
 */
