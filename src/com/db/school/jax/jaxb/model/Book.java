package com.db.school.jax.jaxb.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

	@XmlAttribute
	private Integer id;
	
	@XmlElement
	private String author;
	
	@XmlElement
	private String title;
	
	@XmlElement
	private String genre;
	
	@XmlElement
	private Double price;
	
	@XmlElement(name="publish_date")
	private Date publishDate;
	
	@XmlElement
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "Book #"+id+"[author=" + author + ", title=" + title + ", genre="
				+ genre + ", price=" + price + ", publishDate=" + publishDate
				+ ", description=" + description + "]";
	}
	
}
