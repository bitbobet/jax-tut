package com.db.school.jax.jaxb;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import com.db.school.jax.jaxb.model.Book;
import com.db.school.jax.jaxb.model.Catalog;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class JAXBValidationExample {

	public static void main(String[] args) {
		File file = new File("books.xml");
		try {
			//JAXBContext provides the entry point to the JAXB API:
	        JAXBContext context = JAXBContext.newInstance(Catalog.class);
	        
	        //This object controls the process of unmarshalling (read xml into java objects)
	        Unmarshaller reader = context.createUnmarshaller();
	        
	        SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
	        try {
	            Schema schema = sf.newSchema(new File("books.xsd"));
	            /**
	             * Setting the schema automatically enables validation (since JAXB 2.0)
	             * The default configuration causes the unmarshal operation to fail upon encountering the first validation error
	             */
	            reader.setSchema(schema);
	            reader.setEventHandler(
	                new ValidationEventHandler() {
	                    public boolean handleEvent(ValidationEvent ve) {
	                        // ignore warnings
	                        if (ve.getSeverity() != ValidationEvent.WARNING) {
	                            ValidationEventLocator vel = ve.getLocator();
	                            System.out.println("Line:Col[" + vel.getLineNumber() +
	                                ":" + vel.getColumnNumber() +
	                                "]:" + ve.getMessage());
	                        }
	                        return true;
	                    }
	                }
	            );
	        } catch (SAXException se) {
	            System.out.println("Unable to validate due to following error.");
	            se.printStackTrace();
	        }
	        
	        //unmarshal method does the actual unmarshalling of the XML document represented by the root element (catalog)
	        Catalog catalog = (Catalog) reader.unmarshal(file);
	        for(Book book : catalog.getBooks()){
	        	System.out.println(book);
	        }
	        
		} catch( UnmarshalException e ) {
			System.out.println("ValidationEventHandler stopped processing due to an invalid type");
		} catch (JAXBException e) {
			e.printStackTrace();
		} 
	}
}
